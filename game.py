# -*- coding: utf-8 -*-
import itertools
import gym
import random

from util import str_intarray


class Game(gym.Env):
    def __init__(self, alphabet_num: int = 10, word_len: int = 4):
        self.alphabets = range(alphabet_num)
        self.bag = list(itertools.permutations(self.alphabets, word_len))
        self.action_space = gym.spaces.multi_discrete.MultiDiscrete((alphabet_num,) * word_len)
        self.answer = random.choice(self.bag)
        self.history = []

    def step(self, action: list):
        observation = self.ab(action)
        a, b = observation
        reward = (2 * a + b - 4.0) / 4
        if observation == (4, 0):
            done = True
        else:
            done = False
        self.history.append((action, observation))
        info = None
        return observation, reward, done, info

    def reset(self):
        self.answer = random.choice(self.bag)
        self.history.clear()
        return 0, 0

    def render(self, mode='human'):
        print(self.history)

    def ab(self, w: list):
        a = 0
        for i, j in zip(w, self.answer):
            if i == j:
                a += 1
        b = len(set(w).intersection(set(self.answer)))
        return a, b - a


if __name__ == '__main__':
    game = Game()
    done = False
    while not done:
        action = str_intarray(input('Guess: '))
        obs, reward, done, _ = game.step(action)
        a, b = obs
        print('%iA%iB - r=%f' % (a, b, reward))
