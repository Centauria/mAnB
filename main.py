# -*- coding: utf-8 -*-
import numpy as np
import itertools
import torch
import torch.nn as nn
import torch.optim as opt

from game import Game
from ai import AI
from memory import Memory

alphabet_num = 10
word_length = 4
batch_size = 1

game = Game(alphabet_num, word_length)
game.reset()
print(game.answer)

model: AI = AI()
memory = Memory(10000, (word_length,), (2,))

loss_function = nn.NLLLoss()
optimizer = opt.SGD(model.parameters(), lr=0.1)


def train_model_policy_gradient(model: AI, n_episode: int):
    for episode in range(n_episode):
        observation = game.reset()
        init = torch.ones(word_length, batch_size, alphabet_num, dtype=torch.float) / alphabet_num
        while True:
            out, hidden = model(init, model.init_hidden(batch_size))
            action = torch.argmax(out, dim=-1).view(-1)
            observation_, reward, done, _ = game.step(action.tolist())
            if done:
                pass
