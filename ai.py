# -*- coding: utf-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F


class AI(nn.Module):
    def __init__(self, alphabet_num: int = 10, word_len: int = 4, hidden_size: int = 20):
        super(AI, self).__init__()
        self.alphabet_num = alphabet_num
        self.word_len = word_len
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(alphabet_num, hidden_size, word_len)
        self.hidden_to_probability = nn.Linear(hidden_size, alphabet_num)

    def forward(self, word, hidden):
        # word: (word_len, batch_size, alphabet_num)
        lstm_out, hid = self.lstm(word, hx=hidden)
        # lstm_out: (word_len, batch_size, hidden_size)
        prob_out = self.hidden_to_probability(lstm_out)
        # prob_out: (word_len, batch_size, alphabet_num)
        prob_out = F.log_softmax(prob_out, dim=-1)
        return prob_out, hid

    def init_hidden(self, batch_size=1):
        c = torch.zeros(self.word_len, batch_size, self.hidden_size)
        h = torch.zeros(self.word_len, batch_size, self.hidden_size)
        return c, h
