# -*- coding: utf-8 -*-
def str_intarray(s: str):
    return list(map(int, list(s)))
