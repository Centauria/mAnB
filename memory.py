# -*- coding: utf-8 -*-
import numpy as np


class RingBuffer(object):
    def __init__(self, max_len, shape, dtype='float32'):
        self.max_len = max_len
        self.start = 0
        self.length = 0
        self.data = np.zeros((max_len,) + shape).astype(dtype)

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        if idx < 0 or idx >= self.length:
            raise KeyError()
        return self.data[(self.start + idx) % self.max_len]

    def get_batch(self, indexes):
        return self.data[(self.start + indexes) % self.max_len]

    def append(self, v):
        if self.length < self.max_len:
            # We have space, simply increase the length.
            self.length += 1
        elif self.length == self.max_len:
            # No space, "remove" the first item.
            self.start = (self.start + 1) % self.max_len
        else:
            # This should never happen.
            raise RuntimeError()
        self.data[(self.start + self.length - 1) % self.max_len] = v


def array_min2d(x):
    x = np.array(x)
    if x.ndim >= 2:
        return x
    return x.reshape(-1, 1)


class Memory(object):
    def __init__(self, limit, action_shape, observation_shape):
        self.limit = limit

        self.observations0 = RingBuffer(limit, shape=observation_shape)
        self.actions = RingBuffer(limit, shape=action_shape)
        self.rewards = RingBuffer(limit, shape=(1,))
        self.terminals1 = RingBuffer(limit, shape=(1,))
        self.observations1 = RingBuffer(limit, shape=observation_shape)

    def sample(self, batch_size):
        # Draw such that we always have a proceeding element.
        batch_indexes = np.random.randint(self.nb_entries - 2, size=batch_size)

        obs0_batch = self.observations0.get_batch(batch_indexes)
        obs1_batch = self.observations1.get_batch(batch_indexes)
        action_batch = self.actions.get_batch(batch_indexes)
        reward_batch = self.rewards.get_batch(batch_indexes)
        terminal1_batch = self.terminals1.get_batch(batch_indexes)

        result = {
            'obs0': array_min2d(obs0_batch),
            'obs1': array_min2d(obs1_batch),
            'rewards': array_min2d(reward_batch),
            'actions': array_min2d(action_batch),
            'terminals1': array_min2d(terminal1_batch),
        }
        return result

    def append(self, obs0, action, reward, obs1, terminal1, training=True):
        if not training:
            return

        self.observations0.append(obs0)
        self.actions.append(action)
        self.rewards.append(reward)
        self.observations1.append(obs1)
        self.terminals1.append(terminal1)

    @property
    def nb_entries(self):
        return len(self.observations0)
